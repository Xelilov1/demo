FROM alpine:3.11.2
RUN apk add --no-cache openjdk8
COPY build/libs/demo-0.0.1-SNAPSHOT.jar /app/
EXPOSE 9090
WORKDIR /app/
#ENTRYPOINT ["java"]
CMD ["java","-jar","/app/demo-0.0.1-SNAPSHOT.jar"]


