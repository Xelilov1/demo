package com.example.demo;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;

@RestController
public class Controller {
    @GetMapping
    public String test(){
        return "Salam "+ LocalDateTime.now();
    }
}
